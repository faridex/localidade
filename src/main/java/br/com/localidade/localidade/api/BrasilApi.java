package br.com.localidade.localidade.api;

import java.util.ArrayList;
import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.localidade.localidade.vo.LatLongVo;


/**
 * BrasilApi
 */

 @RestController
 @RequestMapping("/brasil")
public class BrasilApi {

    @GetMapping(value="/capitais")
    public List<LatLongVo> getMethodName() {
        List<LatLongVo> latlongs = new ArrayList<LatLongVo>();    

        latlongs.add(new LatLongVo("AC","-8.77","-70.55"));
        latlongs.add(new LatLongVo("AL","-9.71","-35.73"));
        latlongs.add(new LatLongVo("AM","-3.07","-61.66"));
        latlongs.add(new LatLongVo("AP"," 1.41","-51.77"));
        latlongs.add(new LatLongVo("BA","12.96","-38.51"));
        latlongs.add(new LatLongVo("CE","-3.71","-38.54"));
        latlongs.add(new LatLongVo("DF","15.83","-47.86"));
        latlongs.add(new LatLongVo("ES","19.19","-40.34"));
        latlongs.add(new LatLongVo("GO","16.64","-49.31"));
        latlongs.add(new LatLongVo("MA","-2.55","-44.30"));
        latlongs.add(new LatLongVo("MT","12.64","-55.42"));
        latlongs.add(new LatLongVo("MS","20.51","-54.54"));
        latlongs.add(new LatLongVo("MG","18.10","-44.38"));
        latlongs.add(new LatLongVo("PA","-5.53","-52.29"));
        latlongs.add(new LatLongVo("PB","-7.06","-35.55"));
        latlongs.add(new LatLongVo("PR","24.89","-51.55"));
        latlongs.add(new LatLongVo("PE","-8.28","-35.07"));
        latlongs.add(new LatLongVo("PI","-8.28","-43.68"));
        latlongs.add(new LatLongVo("RJ","22.84","-43.15"));
        latlongs.add(new LatLongVo("RN","-5.22","-36.52"));
        latlongs.add(new LatLongVo("RO","11.22","-62.80"));
        latlongs.add(new LatLongVo("RS","30.01","-51.22"));
        latlongs.add(new LatLongVo("RR"," 1.89","-61.22"));
        latlongs.add(new LatLongVo("SC","27.33","-49.44"));
        latlongs.add(new LatLongVo("SE","10.90","-37.07"));
        latlongs.add(new LatLongVo("SP","23.55","-46.64"));
        latlongs.add(new LatLongVo("TO","10.25","-48.25"));
        return latlongs;
    }
    
    
}