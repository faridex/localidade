package br.com.localidade.localidade.vo;


/**
 * LatLongVo
 */
public class LatLongVo {

    private String coUf;
    private String coLatitude;
    private String coLongitude;


    
    public String getCoUf() {
        return coUf;
    }

    public void setCoUf(String coUf) {
        this.coUf = coUf;
    }

    public String getCoLatitude() {
        return coLatitude;
    }

    public void setCoLatitude(String coLatitude) {
        this.coLatitude = coLatitude;
    }

    public String getCoLongitude() {
        return coLongitude;
    }

    public void setCoLongitude(String coLongitude) {
        this.coLongitude = coLongitude;
    }

    public LatLongVo(String coUf, String coLatitude, String coLongitude) {
        this.coUf = coUf;
        this.coLatitude = coLatitude;
        this.coLongitude = coLongitude;
    }

    


    
}